#coding=utf-8
import types
import pydsh.command as command
import pydsh.commons.resolvers as resolvers
from multiprocessing import Process, Queue

__namespace = 'p'

class _EOF:
    pass

class _Wrapper:
    def __init__(self, function, queue):
        self.function, self.queue = function, queue

    def send(self, object):
        if isinstance(object, types.GeneratorType):
            for i in object:
                self.send(i)
        else:
            self.queue.put(object)

    def __call__(self, *args, **kwargs):
        result = self.function()
        self.send(result)
        self.send(_EOF())

@command.nofork
def run(*args):
    """Run in parallel"""

    processes, queue = list(), Queue()
    for arg in args:
        processes.append(Process(target=_Wrapper(arg, queue)))
        processes[-1].start()

    running = len(processes)
    while running > 0:
        result = queue.get()
        if isinstance(result, _EOF):
            running -= 1
        else:
            yield result

    for p in processes:
        p.join()