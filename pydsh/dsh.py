# coding=utf-8
import operator
import types
import grako
import sys

from util import escape, boolean, exceptions
from pydsh import grammar
from process.interrupt import Interrupt

def execute(self, ast):
    def _call_printer(res):
        if isinstance(res, types.GeneratorType):
            for x in res:
                _call_printer(x)
        else:
            self.printer(res)
    try:
        result = self._compile(ast)()
        if isinstance(result, types.FunctionType):
            result = result()
        _call_printer(result)
    except AttributeError as e:
        self.printer(e.message, True)
    except ValueError as e:
        self.printer(e.message, True)
    except KeyError as e:
        self.printer(e.args[0] if len(e.args) == 1 else (e.args[1], e.args[2]), True)
    except TypeError as e:
        self.printer(e.args[0], True)
    except IndexError as e:
        self.printer(e.message, True)
    except:
        raise

class Script(object):
    def __init__(self, shell, script):
        self.script, self.shell = script, shell

    def __call__(self):
        for s in self.script:
            yield s()

class Condition(object):
    def __init__(self, fn):
        self.fn = fn

    def __call__(self):
        return self.fn()

class Shell(escape.BalancedReader, grammar.dslParser):
    VAR = "var"
    VAL = "val"
    LVAL = "lval"
    RVAL = "rval"
    BOOL = "bool"
    CAST = "cast"
    VALUE = "value"
    SCRIPT = "script"
    OPERAND = "operand"
    COMMAND = "command"
    ARGUMENTS = "arguments"
    EXPRESSION = "expression"

    OPERATORS = {
        '<'  : operator.lt,
        '>'  : operator.gt,
        '<=' : operator.le,
        '>=' : operator.ge,
        '==' : operator.eq,
        '!=' : operator.ne
    }

    NUMERIC_TYPES = [int,float]

    def __init__(self, interpreter, prompt="> ", server=False, printer=None, done=None):
        super(Shell, self).__init__(parseinfo=False, prompt=prompt)

        self.variables, self.has_cast, self.server = {}, False, server
        self.interpreter = interpreter

        if printer:
            setattr(self, 'printer', printer)
        if done:
            setattr(self, 'done', done)

    def printer(self, value, error = False):
        if not value and type(value) != int and type(value) != float and type(value) != str:
            return
        elif isinstance(value,types.TupleType):
            print " ".join(map(str,value))
        else:
            print value #, type(value)

    def done(self):
        pass

    def _cleanup(self, ast):
        result = ast
        if type(ast) == list:
            result = list(ast)
            for i in range(0, len(ast)):
                result[i] = self._cleanup(ast[i])
        elif type(ast) == dict or type(ast) == grako.ast.AST:
            result = dict(ast)
            for k, v in ast.items():
                result[k] = self._cleanup(v)
        elif type(ast) == str or type(ast) == unicode:
            result = ast.decode('script_escape')
        return result

    def _resolve_value(self, value):
        if type(value) == unicode or type(value) == str:
            if value[0] == '$':
                def _exec():
                    try:
                        return self.variables[value[1:]]
                    except KeyError:
                        raise IndexError("Variable $" + value[1:] + " not found")
                return _exec
            else:
                return value

    def _compile_boolean_part(self,ast):
        return ast if type(ast) == str or type(ast) == unicode else self._compile_boolean(ast)

    @staticmethod
    def _try_boolean_typecast(l,r,l_cast,r_cast):
        ltype, rtype = type(l), type(r)
        with exceptions.ignore(ValueError):
            if not r_cast and ltype in Shell.NUMERIC_TYPES and rtype not in Shell.NUMERIC_TYPES:
                r = ltype(r)
            elif not l_cast and rtype in Shell.NUMERIC_TYPES and ltype not in Shell.NUMERIC_TYPES:
                l = rtype(l)
        return l,r

    def _compile_boolean(self,ast):
        if type(ast) == dict:
            if ast.get(Shell.EXPRESSION):
                lval = self._compile_arg(ast[Shell.EXPRESSION][Shell.LVAL], boolean = True)
                l_cast = self.has_cast
                rval = self._compile_arg(ast[Shell.EXPRESSION][Shell.RVAL], boolean = True)
                r_cast = self.has_cast
                def _exec():
                    return Shell.OPERATORS[ast[Shell.EXPRESSION][Shell.OPERAND]](
                        *(Shell._try_boolean_typecast(lval(),rval(),l_cast,r_cast))
                    )
                return _exec
            elif ast.get(Shell.COMMAND):
                return self._compile(ast[Shell.COMMAND], boolean = True)
        elif type(ast) == list:
            parts = map(self._compile_boolean_part, ast)
            parts = reduce(lambda x, y : x.add(y), parts, boolean.Not())
            parts = reduce(lambda x, y : x.add(y), parts, boolean.And()).done()
            parts = reduce(lambda x, y : x.add(y), parts, boolean.Or()).done()
            assert len(parts) == 1
            return parts[0]

    @staticmethod
    def _wrap_value(v):
        if isinstance(v, types.FunctionType):
            return v

        def _wrap():
            return v

        return _wrap

    @staticmethod
    def _wrap_cast(fn, v):
        return fn(v() if not isinstance(v, Script) else v)

    def _compile_arg(self, ast, **kwargs):
        def _typecast(v):
            return v

        typecast = (_typecast if Shell.CAST not in ast else self.interpreter.get_cast(ast[Shell.CAST]) or _typecast)
        result, self.has_cast = None,(typecast != _typecast)
        if Shell.VALUE in ast:
            def _exec():
                v = Shell._wrap_cast(typecast, Shell._wrap_value(self._resolve_value(ast[Shell.VALUE])))
                if isinstance(v,list):
                    def _make_generator():
                        for i in v:
                            yield i
                    return _make_generator()
                return v
            result = _exec
        elif Shell.COMMAND in ast:
            def _exec():
                return Shell._wrap_cast(typecast, Shell._wrap_value(self._compile(ast[Shell.COMMAND], **kwargs)))
            result = _exec
        elif Shell.SCRIPT in ast:
            def _exec():
                return Shell._wrap_cast(typecast, Script(self, map(self._compile, ast[Shell.SCRIPT])))
            result = _exec
        elif Shell.BOOL in ast:
            def _exec():
                return Condition(self._compile_boolean(ast[Shell.BOOL]))
            result = _exec
        return result

    def _no_fork(self, ast):
        assert type(ast) == dict
        if Shell.COMMAND not in ast:
            return False
        fn = self.interpreter[ast[Shell.COMMAND]]
        return fn and hasattr(fn, 'func_closure') \
               and len(fn.func_closure) > 0 \
               and hasattr(fn.func_closure[0].cell_contents, '_nofork') \
               and getattr(fn.func_closure[0].cell_contents, '_nofork')

    def _compile(self, ast, **kwargs):
        assert type(ast) == dict
        if Shell.COMMAND in ast:
            fn = self.interpreter[ast[Shell.COMMAND]]
            if not fn:
                raise AttributeError("command not found: " + ast[Shell.COMMAND])
            args = map(self._compile_arg, ast[Shell.ARGUMENTS])
            def _exec():
                return fn(*(map(lambda x: x(), args)), **dict(kwargs, environment = self.variables))
            return _exec
        elif Shell.VAR in ast:
            compiled = self._compile_arg(ast[Shell.VAL])
            def _exec():
                val = compiled()
                self.variables[ast[Shell.VAR]] = [x for x in val] if isinstance(val, types.GeneratorType) else val
            return _exec

    def push(self, queue):
        queue.put(self.variables)

    def merge(self, state):
        for k in [k for k in self.variables.keys() if k not in state]:
            del self.variables[k]
        self.variables.update(state)

    def execute(self, s = None):
        if self.script or s:
            try:
                ast = self._cleanup(self.parse(s if s else self.script, "COMMAND"))
                if self.server or sys.platform == 'win32' or self._no_fork(ast):
                    try:
                        execute(self,ast)
                    except KeyboardInterrupt:
                        pass #let it go on Windows...
                else:
                    Interrupt(target=execute, state=self, args=(ast,)).merge()
            except grako.exceptions.FailedParse as e:
                self.printer(e.message, True)
        self.done()

    def cmdloop(self, input = raw_input):
        while True:
            try:
                line = input(self.current_prompt)
                self.script = (line if self.script == None else self.script + ("\n" + line)).strip()
                self._balance()
            except EOFError:
                break
