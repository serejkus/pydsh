# coding=utf-8
from operator import itemgetter as _itemgetter, eq as _eq
from keyword import iskeyword as _iskeyword
from collections import OrderedDict
import sys as _sys

_repr_template = '{name}=%r'

_field_template = '''\
    {name} = _property(_itemgetter({index:d}), doc='Alias for field number {index:d}')
'''

_class_template = '''\
class {typename}(tuple):
    '{typename}({arg_list})'

    _fields = {field_names!r}

    def __new__(_cls, {arg_list}):
        return _tuple.__new__(_cls, ({arg_list_nodef}))

    def __repr__(self):
        return '{typename}({repr_fmt})' % self

    def _asdict(self):
        return OrderedDict(zip(self._fields, self))

    def _replace(_self, **kwds):
        result = _self._make(map(kwds.pop, {field_names!r}, _self))
        if kwds:
            raise ValueError('Got unexpected field names: %r' % kwds.keys())
        return result

    def __getnewargs__(self):
        return tuple(self)

    __dict__ = _property(_asdict)

    def __getstate__(self):
        pass

{field_defs}
'''

def mynamedtuple(typename, field_names, default_values = {}, verbose=False):
    # Validate the field names.  At the user's option, either generate an error
    # message or automatically replace the field name with a valid name.
    if isinstance(field_names, basestring):
        field_names = field_names.replace(',', ' ').split()
    field_names = map(str, field_names)
    for name in [typename] + field_names:
        if not all(c.isalnum() or c=='_' for c in name):
            raise ValueError('Type names and field names can only contain '
                             'alphanumeric characters and underscores: %r' % name)
        if _iskeyword(name):
            raise ValueError('Type names and field names cannot be a '
                             'keyword: %r' % name)
        if name[0].isdigit():
            raise ValueError('Type names and field names cannot start with '
                             'a number: %r' % name)
    seen = set()
    for name in field_names:
        if name.startswith('_'):
            raise ValueError('Field names cannot start with an underscore: '
                             '%r' % name)
        if name in seen:
            raise ValueError('Encountered duplicate field name: %r' % name)
        seen.add(name)

    # Fill-in the class template
    class_definition = _class_template.format(
        typename = typename,
        field_names = tuple(field_names),
        num_fields = len(field_names),
        arg_list = ", ".join(["{0}{1}".format(x,"" if x not in default_values else " = {0}".format(default_values[x])) for x in field_names]),
        arg_list_nodef = repr(tuple(field_names)).replace("'", "")[1:-1],
        repr_fmt = ', '.join(_repr_template.format(name=name) for name in field_names),
        field_defs = '\n'.join(_field_template.format(index=index, name=name) for index, name in enumerate(field_names))
    )
    if verbose:
        print class_definition

    namespace = dict(_itemgetter=_itemgetter, __name__='namedtuple_%s' % typename,
                     OrderedDict=OrderedDict, _property=property, _tuple=tuple)
    try:
        exec class_definition in namespace
    except SyntaxError as e:
        raise SyntaxError(e.message + ':\n' + class_definition)
    result = namespace[typename]

    try:
        result.__module__ = _sys._getframe(1).f_globals.get('__name__', '__main__')
    except (AttributeError, ValueError):
        pass

    return result
