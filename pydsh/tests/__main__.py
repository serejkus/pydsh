# coding=utf-8
import os, sys
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))

from test_arguments  import *
from test_modules    import *
from test_options    import *
from test_validators import *

if __name__ == "__main__":
    unittest.main()