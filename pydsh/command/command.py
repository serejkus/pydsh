# coding=utf-8
import inspect, types, argument, argparser
from collections import namedtuple
import pydsh.util.errors as errors
import pydsh.util.exceptions as exceptions

CommandBase = namedtuple('CommandBase',['wrapped'])
CommandContext = namedtuple('CommandContext', ['env', 'bool'])

CONTEXT_ARGUMENT_NAME = '_context'

def make_context(kwargs):
    return CommandContext(env = kwargs["environment"], bool = "boolean" in kwargs and kwargs["boolean"])

class ZeroArgumentCommand(CommandBase):
    @staticmethod
    def call_0(fn, *args, **_):
        return fn(*args)

    @staticmethod
    def call_0C(index):
        def _caller(fn, *args, **kwargs):
            return fn(*(args[0:index] + (make_context(kwargs),) + args[index:]))
        return _caller

    def __init__(self, fn):
        """
        Represents simple command without named arguments.
        Can wrap functions with following signatures:

        1. ``def echo([self])...``
        2. ``def echo([self,] context)...``
        3. ``def echo([self,] *args)...``
        4. ``def echo([self,] context, *args)...``

        :param fn: implementation of the function
        """
        args = inspect.getargspec(fn)
        assert not args.keywords
        self.caller = ZeroArgumentCommand.call_0 if CONTEXT_ARGUMENT_NAME not in args.args else ZeroArgumentCommand.call_0C(args.args.index(CONTEXT_ARGUMENT_NAME))

    def __call__(self, *args, **kwargs):
        return self.caller(self.wrapped, *args, **kwargs)

    @property
    def __doc__(self):
        return (self.wrapped.__doc__,None)

class MultipleArgumentCommand(CommandBase):
    class Builder(object):
        def __init__(self, *args):
            self.args = args

        def __call__(self, fn):
            result = MultipleArgumentCommand(fn, self.args)
            if hasattr(self, '_nofork'):
                setattr(result, '_nofork', getattr(self, '_nofork'))
            return result

    @staticmethod
    def call_A(obj, index):
        def _caller(fn, *args, **kwargs):
            if obj.parser: #We have options.
                fnArgs, aIndex, indx = [], 0, 0
                #Append self and / or _context
                while len(fnArgs) < obj.argspecs[0].index:
                    if len(fnArgs) == index:
                        fnArgs.append(make_context(kwargs))
                    else:
                        fnArgs.append(args[aIndex])
                        aIndex += 1
                #Parse command line arguments
                opts, args = obj.parser.parse_args(args[aIndex:])
                for arg in obj.argspecs:
                    #Append context if necessary
                    if len(fnArgs) == index:
                        fnArgs.append(make_context(kwargs))
                    #Resolve argument/option value
                    if isinstance(arg, argument.Option):
                        value = opts[arg.name]
                    elif indx < len(args):
                        value = args[indx]
                        indx += 1
                    else:
                        break
                    #Validate
                    if not arg.validate:
                        fnArgs.append(value)
                    else:
                        with exceptions.ignore(IndexError):
                            if not (isinstance(arg.validate, types.FunctionType) or isinstance(arg.validate, staticmethod)):
                                errors.check(value, arg.validate)
                                fnArgs.append(value)
                            else:
                                assert isinstance(arg, argument.Argument)
                                args = args[:indx-1] + args[indx-1+errors.check(args[indx-1:],arg.validate):]
                                fnArgs.append(args[indx-1])
                return fn(*(fnArgs + list(args[indx:])))
            else: #Only arguments. Call function as is
                for arg in obj.argspecs:
                    if not arg.validate:
                        continue
                    with exceptions.ignore(IndexError):
                        ai = arg.index if index < 0 or arg.index < index else arg.index - 1
                        if not (isinstance(arg.validate, types.FunctionType) or isinstance(arg.validate, staticmethod)):
                            errors.check(args[ai], arg.validate)
                        else:
                            args = args[:ai] + args[ai+errors.check(args[arg.index:], arg.validate):]
                return fn(*(args[0:index] + (make_context(kwargs),) + args[index:])) if index >= 0 else fn(*args)
        return _caller

    @staticmethod
    def _transform(args):
        def _transformer(x):
            if x.name not in args:
                raise LookupError("No such argument: " + x.name)
            elif x.name == CONTEXT_ARGUMENT_NAME:
                raise KeyError("Cannot have argument named {0}".format(CONTEXT_ARGUMENT_NAME))
            elif isinstance(x, argument.Argument):
                return argument.Argument(**dict(name = x.name, help = x.help, validate = x.validate, index = args.index(x.name)))
            assert isinstance(x, argument.Option)
            if x.short:
                assert len(x.short) == 1
            return argument.Option(**dict(name = x.name, short = x.short, help = x.help, validate = x.validate, index = args.index(x.name)))
        return _transformer

    def __new__(clazz, fn, args):
        return super(MultipleArgumentCommand,clazz).__new__(clazz, fn)

    def __init__(self, _, argspecs):
        """
        Represents command with named arguments.
        Can wrap functions with following signatures:

        1. ``def echo([self,] [args,...])...``
        2. ``def echo([self,] [args,...] context)...``
        3. ``def echo([self,] [args,...] *args)...``
        4. ``def echo([self,] [args,...] context, *args)...``

        :param fn: implementation of function
        """
        args = inspect.getargspec(self.wrapped)
        assert args.keywords == None

        self.argspecs = tuple(sorted(
            map(MultipleArgumentCommand._transform(args.args), argspecs),
            lambda a,b : cmp(a.index,b.index) if type(a) == type(b) else (-1 if isinstance(a,argument.Option) else 1)
        ))
        self.caller = MultipleArgumentCommand.call_A(self, -1 if CONTEXT_ARGUMENT_NAME not in args.args else args.args.index(CONTEXT_ARGUMENT_NAME))
        self.documentation = ''

        _opts = filter(lambda x : isinstance(x,argument.Option),   self.argspecs)
        _args = filter(lambda x : isinstance(x,argument.Argument), self.argspecs)

        if len(_opts):
            self.documentation += "\nOptions:"
            arglen = max(map(lambda x: len(x.name) + (3 if x.short else 0), _opts))
            self.parser = argparser.ArgumentParser(add_help = False)
            for opt in _opts:
                _aargs = ["--{0}".format(opt.name)]
                _akwar = {}
                self.documentation += "\n  --{0}".format(opt.name)
                if opt.short:
                    self.documentation += ",-{0}".format(opt.short)
                    _aargs.append("-{0}".format(opt.short))
                if opt.help:
                    self.documentation += (" " * (arglen - (len(opt.name) + (3 if opt.short else 0)))) + " - " + opt.help
                if args.defaults:
                    defaultIndex = (opt.index - (len(args.args) - len(args.defaults)))
                    if defaultIndex >= 0 and defaultIndex < len(args.defaults):
                        d = args.defaults[defaultIndex]
                        if opt.help:
                            self.documentation += " (default " + str(d) + ")"
                        _akwar.update(dict(default = d))
                        if type(d) == bool:
                            _akwar.update(dict(action = 'store_false' if d else 'store_true'))
                if opt.validate and (list == opt.validate or (type(opt.validate) == list and list in opt.validate)):
                    _akwar.update(dict(nargs = '+'))
                self.parser.add_argument(*_aargs, **_akwar)
            self.parser.add_argument('arguments', metavar = 'A', nargs = '+')
        else:
            self.parser = None

        if len(_args):
            if len(_opts):
                self.documentation += "\n"
            self.documentation += "\nArguments:"
            arglen = max(map(lambda x: len(x.name), _args))
            for arg in _args:
                self.documentation += "\n  {0}".format(arg.name)
                if arg.help:
                    self.documentation += (" " * (arglen - len(arg.name))) + " - " + arg.help
                    if args.defaults:
                        defaultIndex = (arg.index - (len(args.args) - len(args.defaults)))
                        if defaultIndex >= 0 and defaultIndex < len(args.defaults):
                            self.documentation += " (default " + str(args.defaults[defaultIndex]) + ")"

        self.documentation = (self.wrapped.__doc__,self.documentation)
        self.argspecs = tuple(sorted(
            map(MultipleArgumentCommand._transform(args.args), argspecs),
            lambda a,b : cmp(a.index,b.index)
        ))

    def __call__(self, *args, **kwargs):
        return self.caller(self.wrapped, *args, **kwargs)

    @property
    def __doc__(self):
        return self.documentation

def Command(*args):
    if len(args) == 1 and isinstance(args[0], types.FunctionType):
        return ZeroArgumentCommand(args[0])
    return MultipleArgumentCommand.Builder(*args)
