# coding=utf-8
import unittest
import pydsh as sh

class PyDSHTest(unittest.TestCase):
    def __init__(self, interpreter, *args, **kwargs):
        super(PyDSHTest,self).__init__(*args, **kwargs)
        self.shell = sh.Shell(interpreter(), printer=self.handle_output(), server=True)

    def handle_output(self):
        def _impl(output, error = False):
            if output or type(output) in [str,int,float,bool]:
                self.output += [output] if not error else [(output,True)]
        return _impl


    def assertOutput(self, out):
        self.assertListEqual(self.output, out)

    def setUp(self):
        self.output = []