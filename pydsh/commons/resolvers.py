# coding=utf-8
import inspect, types, sys
import pydsh.command as command
from pydsh.command.command import CommandBase

class TypeCast(object):
    Casters = dict(int=int, str=str, float=float)

    def get_cast(self, name):
        return TypeCast.Casters.get(name)

class MapMethodsMetaClass(type):
    def __new__(cls, name, bases, dct):
        #remove MapMethods if we already have MapModules
        i_MapMethods, i_MapModules = -1, -1
        for i in range(0,len(bases)):
            if __name__ == bases[i].__module__ and bases[i].__name__ == 'MapModules':
                i_MapModules = i
            elif __name__ == bases[i].__module__ and bases[i].__name__ == 'MapMethods':
                i_MapMethods = i

        if i_MapMethods >=0 and i_MapModules >= 0:
            bases = bases[0:i_MapMethods] + bases[i_MapMethods+1:]

        dct['_HELPERS_MAP'] = dict()
        dct['_COMMAND_MAP'] = dict()
        return super(MapMethodsMetaClass,cls).__new__(cls,name,bases,dct)

    @staticmethod
    def _wrap_method_command(m, prefix = None):
        def _caller(s):
            def _call_me(*args, **kwargs):
                try:
                    return m(*((s,) + args), **kwargs)
                except TypeError as e:
                    if prefix:
                        e.args = (e.args[0][len(prefix):],)
                    raise e
            return _call_me
        return _caller

    def __init__(cls, name, bases, dct):
        super(MapMethodsMetaClass,cls).__init__(name,bases,dct)
        if hasattr(cls, '_' + name + '__prefix'):
            prefix  = getattr(cls, '_' + name + '__prefix')
            for k, v in filter(lambda (x,y) :
                y.wrapped.__name__[0:len(prefix)] == prefix
                    if isinstance(y,CommandBase) else
                isinstance(y,types.FunctionType) and y.__name__[0:len(prefix)] == prefix, dct.iteritems()
            ):
                dct['_COMMAND_MAP'][k[len(prefix):]] = MapMethodsMetaClass._wrap_method_command(v, prefix)
                dct['_HELPERS_MAP'][k[len(prefix):]] = v.__doc__
        else:
            for k, v in filter(lambda (x,y) : isinstance(y,CommandBase), dct.iteritems()):
                dct['_COMMAND_MAP'][k] = MapMethodsMetaClass._wrap_method_command(v)
                dct['_HELPERS_MAP'][k] = v.__doc__

        for c in filter(lambda x : hasattr(x, '_COMMAND_MAP'), cls.__mro__):
            dct['_COMMAND_MAP'].update(getattr(c, '_COMMAND_MAP'))
            dct['_HELPERS_MAP'].update(getattr(c, '_HELPERS_MAP'))
        setattr(cls, '_HELPERS_LEN', max([len(k) for k,_ in dct['_HELPERS_MAP'].items()] if len(dct['_HELPERS_MAP']) else [0]))

class MapMethods(object):
    __metaclass__ = MapMethodsMetaClass
    __prefix = 'do_'

    def __getitem__(self, item):
        return self.__class__._COMMAND_MAP[item](self) if item in self.__class__._COMMAND_MAP else self.get_command(item)

    @command.nofork(
        command.arg("cmd", help = "Command to print help for")
    )
    def do_help(self, cmd = None):
        """prints help message"""
        if not cmd:
            for k,v in sorted(self.__class__._HELPERS_MAP.items()):
                yield "{0}{2} - {1}".format(k,v[0].split('\n')[0]," " * (self.__class__._HELPERS_LEN - len(k)))
            yield self.print_help(cmd)
        elif cmd in self.__class__._HELPERS_MAP:
            if self.__class__._HELPERS_MAP[cmd][0]:
                yield self.__class__._HELPERS_MAP[cmd][0]
            if self.__class__._HELPERS_MAP[cmd][1]:
                yield self.__class__._HELPERS_MAP[cmd][1]
        else:
            yield self.print_help(cmd)

    def print_help(self, cmd = None):
        if cmd != None:
            raise AttributeError("command not found: " + cmd)

    def get_command(self, item):
        return None

    @staticmethod
    def replace(clz, method, prefix = None):
        name = method.wrapped.__name__
        if prefix:
            assert name[0:len(prefix)] == prefix
            name = name[len(prefix):]
        clz._COMMAND_MAP[name] = MapMethodsMetaClass._wrap_method_command(method, prefix)

class MapModules(MapMethods):
    def __init__(self, modules):
        super(MapModules,self).__init__()
        self._modules = modules
        MapModules.reload.wrapped(self)

    @command.nofork
    def reload(self):
        """reload modules"""
        self._imported = dict()
        try:
            self._modules = map(reload, self._modules)
        except Exception as e:
            return "{0}\n{1}".format(e.__doc__, e.message)

        for module in self._modules:
            namespace = getattr(module,'__namespace') if '__namespace' in dir(module) else None
            commands  = dict(inspect.getmembers(module, lambda x : isinstance(x, CommandBase)))
            self._imported.update(dict([(k if not namespace else namespace + "." + k,v) for k,v in commands.items()]))
        self._help = dict([(k,v.__doc__) for k,v in self._imported.items() if v.__doc__[0] != None])
        self._hlen = max([len(k) for k,_ in self._help.items()] if len(self._help) else [0])

    @staticmethod
    def _sort_help(a,b):
        x = a[0].split('.')
        y = b[0].split('.')
        assert len(x) in [1,2]
        assert len(y) in [1,2]
        if len(x) != len(y):
            return len(x) - len(y)
        elif x[0] == y[0]:
            return cmp(x[1],y[1])
        return cmp(x[0],y[0])

    def print_help(self, cmd = None):
        if not cmd:
            p = None
            for k,v in sorted(self._help.items(), cmp = MapModules._sort_help):
                x = k.split('.')
                if len(x) > 1 and p != x[0]:
                    p = x[0]
                    yield ""
                yield "{0}{2} - {1}".format(k,v[0].split('\n')[0]," " * (max(self._hlen,self.__class__._HELPERS_LEN) - len(k)))
        elif cmd in self._help:
            if self._help[cmd][0]:
                yield self._help[cmd][0]
            if len(self._help[cmd]) > 1 and self._help[cmd][1]:
                yield self._help[cmd][1]

    def get_command(self, item):
        return self._imported[item] if item in self._imported else None