# coding=utf-8
import unittest, base
import pydsh.commons as commons
import pydsh.command as command

class TestInterpreter(commons.MapMethods, commons.TypeCast):
    __prefix = 'cmd_'

    @command(
        command.opt("opt")
    )
    def cmd_a(self, opt):
        yield opt

    @command(
        command.opt("opta", validate = [str, unicode]),
        command.opt("optb", validate = int),
    )
    def cmd_b(self, opta, optb = 0):
        yield opta
        yield optb

    @command(
        command.opt("opta", validate = [str, unicode]),
        command.arg("arga", validate = int)
    )
    def cmd_c(self, opta, arga):
        yield opta
        yield arga

    @command(
        command.opt("opta", validate = bool),
        command.opt("optb", validate = [unicode, str])
    )
    def cmd_d(self, optb, opta = True):
        yield optb
        yield opta

    @command(
        command.opt("opta", validate = bool),
        command.opt("optb", validate = [unicode, str])
    )
    def cmd_e(self, optb, opta = False):
        yield optb
        yield opta

    @command(
        command.opt("opta", validate = list)
    )
    def cmd_f(self, opta):
        yield opta

class TestOptions(base.PyDSHTest):
    def __init__(self, *args, **kwargs):
        super(TestOptions,self).__init__(TestInterpreter, *args, **kwargs)

    def test_Options(self):
        self.shell.execute("a=int:10")

        self.shell.execute("a --opt value")
        self.shell.execute("a --opt $a")

        self.shell.execute("b --opta $a")
        self.shell.execute("b --opta foo")
        self.shell.execute("b --opta foo --optb $a")
        self.shell.execute("b --opta foo --optb foo")

        self.shell.execute("c --opta foo bar")
        self.shell.execute("c --opta foo int:20")

        self.shell.execute("d --opta --optb foo")
        self.shell.execute("d --optb foobar")

        self.shell.execute("e --optb foo --opta")
        self.shell.execute("e --optb foobar")

        self.shell.execute("f --opta x")
        self.shell.execute("f --opta x y")
        self.shell.execute("f --opta x y $a")

        self.assertOutput([
            u'value', 10,

            ('Wrong type. Expecting one of (str,unicode) found [int]', True),
            u'foo', 0, u'foo', 10,
            ('Wrong type. Expecting [int] found [unicode]', True),

            ('Wrong type. Expecting [int] found [unicode]', True),
            u'foo',    20,

            u'foo',    False,
            u'foobar', True,

            u'foo',    True,
            u'foobar', False,

            [u'x'], [u'x', u'y'], [u'x', u'y', 10]
        ])

if __name__ == "__main__":
    unittest.main()