# coding=utf-8
import types
import pydsh

import pydsh.command as command
import pydsh.util.errors as errors
import resolvers

class ControlFlow:
    __metaclass__ =  resolvers.MapMethodsMetaClass
    __prefix = 'do_'

    @staticmethod
    def validate_if(tup):
        if len(tup):
            if len(tup) != 2:
                raise ValueError("Expecting [else] pydsh.Script")
            errors.check(tup[0], "else")
            errors.check(tup[1], pydsh.Script)
            return 1
        return 0

    @staticmethod
    def validate_for(tup):
        if len(tup) != 3:
            raise ValueError("Expecting [in] types.GeneratorType pydsh.Script")
        errors.check(tup[0], "in")
        errors.check(tup[1], types.GeneratorType)
        return 1

    @command(
        command.arg("expr",       validate = pydsh.Condition,  help = "expression to check"),
        command.arg("true_code",  validate = pydsh.Script,     help = "code to run if true"),
        command.arg("false_code", validate = validate_if,      help = "code to run if false")
    )
    def do_if(self, expr, true_code, false_code = None):
        """Operator if\n\nSyntax: if [expr] (code) {else (code)}"""
        if expr():
            yield true_code()
        elif false_code:
            yield false_code()

    @command(
        command.arg("expr", validate = pydsh.Condition, help = "while condition is true"),
        command.arg("code", validate = pydsh.Script,    help = "run this code")
    )
    def do_while(self, expr, code):
        """Operator while\n\nSyntax: while [expr] (code)"""
        while expr():
            yield code()

    @command(
        command.arg("variable",  validate = [str, unicode], help = "name of variable which holds value"),
        command.arg("generator", validate = validate_for,   help = "values generator"),
        command.arg("code",      validate = pydsh.Script,   help = "code to run for each value")
    )
    def do_for(self, variable, generator, code, _context):
        """Operator for\n\nSyntax: for variable in {generator} (code)"""
        for value in generator:
            _context.env[variable] = value
            yield code()

    @command(
        command.arg("code", validate = pydsh.Script, help = "code to execute")
    )
    def do_do(self, code):
        """Group several executions to one script\n\nSyntax: do (code)"""
        yield code()
