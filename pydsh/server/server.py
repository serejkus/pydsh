# coding=utf-8
import socket, ssl, threading, sys, struct
import pydsh

class ServerSertificates(dict):
    def __init__(self, certfile, keyfile, ca_certs):
        self.certfile, self.keyfile, self.ca_certs = certfile, keyfile, ca_certs

    def __setattr__(self, key, value):
        self[key] = value

class PyDSHSSLServer:
    class ServingThread(threading.Thread):
        def __init__(self, interpreter, client_socket, client_address):
            super(PyDSHSSLServer.ServingThread,self).__init__()
            self.interpreter    = interpreter
            self.client_socket  = client_socket
            self.client_address = client_address

        def printer(self):
            def _impl(value, _=False):
                if not value and type(value) != int and type(value) != float and type(value) != str:
                    return
                s_repr = str(value)
                self.client_socket.send(struct.pack('I', 2 + len(s_repr)))
                if len(s_repr):
                    self.client_socket.send(s_repr)
            return _impl

        def readline(self, _):
            data = struct.unpack('I', self.client_socket.recv(4))[0]
            if data == 0:
                raise EOFError()
            data = self.client_socket.recv(data)
            if not data:
                raise EOFError()
            return data

        def done(self):
            self.client_socket.send(struct.pack('I', 1))

        def run(self):
            try:
                pydsh.Shell(
                    self.interpreter(),
                    done=self.done,
                    server=True,
                    printer=self.printer()
                ).cmdloop(self.readline)
            except:
                print >> sys.stderr, sys.exc_info()

    def __init__(self, interpreter, port, certificates):
        self.interpreter, self.port = interpreter, port
        self.certificates = certificates
        self.stopped = False

    def serve(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind(('0.0.0.0', self.port))
        sock = ssl.wrap_socket(sock, server_side=True, cert_reqs=ssl.CERT_REQUIRED, **self.certificates)
        sock.listen(5)

        while not self.stopped:
            try:
                cs, ca = sock.accept()
                if self.stopped:
                    continue
                PyDSHSSLServer.ServingThread(self.interpreter, cs, ca).start()
            except ssl.SSLError as ssl_e:
                print ssl_e
