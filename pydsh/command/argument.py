# coding=utf-8
from pydsh.util.namedtuple import mynamedtuple

Argument = mynamedtuple('Argument',["name", "validate", "help","index"], dict(validate = None, help = None, index = -1))
Option   = mynamedtuple('Option',  ["name", "short", "validate", "help", "index"], dict(short = None, validate = None, help = None, index = -1))